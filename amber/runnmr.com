#! /bin/csh -f

#source amber.csh

setenv start_no 1
setenv end_no 20
set nbproc=12

#########################################################
# Create distance restraints file
#########################################################


makeDIST_RST  -ual ../NOEeNOEcombined.constr -pdb 1_anneal.pdb -rst RST -map map.DG-AMBER_cyana
makeDIST_RST -upb ../hb.upl -pdb 1_anneal.pdb -rst RSThb -map map.DG-AMBER_cyana
#makeANG_RST -pdb 1_anneal_copy.pdb -con ../couplings.aco -lib ompatordef.lib > aco.rst -report
cat RSThb >> RST

rm -rf splitstates

#########################################################
# Split the states from CYANA
#########################################################

if ( ! -d splitstates ) then
    mkdir splitstates
endif

if ( -d splitstates ) then
    cd splitstates
    /opt/amber20/autosetup/splitpdb ../../finalNOEeNOEcombined.pdb
    cd ..
endif

while ($start_no <= $end_no)
        if (! -d $start_no) then
	    mkdir $start_no
	endif
	
        cd $start_no
	if ($start_no < 10) then
	    cp ../splitstates/cycle00$start_no.pdb start.pdb
	else
	    if ($start_no < 100) then
	        cp ../splitstates/cycle0$start_no.pdb start.pdb
	    else
        	cp ../splitstates/cycle$start_no.pdb start.pdb
	    endif
	endif	

	# remove pseudoatoms QL1, QL2, QL3
	grep -v Q start.pdb > startQ.pdb
	#sed 's/HG12\ IML/HG13\ IML/g' tmp.pdb | sed 's/HG11\ IML/HG12\ IML/g' > startQ.pdb
	
########################################################
# create input coordinates and prmtop using GAFF
########################################################
	
cat <<EOF > tleap.in

source leaprc.protein.ff14SB
loadAmberPrep ../SAR.prepin
loadAmberPrep ../MVA.prepin
loadAmberPrep ../BMT.prepin
loadAmberPrep ../ABA.prepin
loadAmberPrep ../DAL.prepin
loadAmberPrep ../MLE.prepin

#loadAmberParams ../frcmod.SAR
#loadAmberParams ../frcmod.MVA

loadAmberParams ../frcmod.CH3Namide

# Define sequence for cyclic peptide without terminal residues
seq = { BMT ABA SAR MLE VAL MLE ALA DAL MLE MLE MVA }

# Load structure of cyclic peptide
cyclicpeptide = loadPDBusingseq startQ.pdb seq

# Form cyclic peptide bond
bond cyclicpeptide.1.N cyclicpeptide.11.C

saveAmberParm cyclicpeptide prmtop start.inpcrd
quit

EOF

########################################################
# simple minimization script
########################################################

cat << EOF > min.in

Initial minimisation of macrocycle
 &cntrl
  nmropt = 1, pencut=-0.001,
  imin=1,			! minimization
  maxcyc=1000,  		! total number of cycle
  ncyc=500,			! number of conjugate gradient cycles
  drms=1E-4,			! rms for convergence, default = 1E-4
  cut=16,			! cutoff for non-bonded interactions
  ntb=0,			! non-periodic, ntb=1 constant volume periodic boundary conditions
  igb=1,			! choose radii for gBPB, default ibg=1 "mbondi2" which is the second modification of the Bondi radii set
  extdiel=3.9,			! averaged dielectric constant, 30 % hexadecane = 2.05, 70 % chloroform = 4.71, 
  gbsa=1,			! 0: no SASA-based nonpolar free energy of solvation is used, 1: SASA is approximated using LCPO
/

&ewald                          ! distance dependent dielectric constant, ewald has no effect on the resulting structure

  eemeth=5,
/
 &end

&wt type='REST', istep1=0, istep2=1000, value1=1.0, value2=1.0  &end
&wt type='IMPROP', istep1=0,istep2=1000,value1=1, value2=50 &end
&wt type='BOND', istep1=0,istep2=1000,value1=1, value2=1 &end
&wt type='ANGLE', istep1=0,istep2=1000,value1=1, value2=1 &end
&wt type='TORSION', istep1=0,istep2=1000,value1=1, value2=1.0 &end                                      
&wt type='VDW', istep1=0,istep2=1000,value1=1, value2=1 &end                                      

&wt type='END' &end
LISTOUT=POUT
DISANG=../RST

EOF

########################################################
# simulated annealing
########################################################

cat << EOF > anneal.in

#simulated annealing protocol, 30 ps, in implicit water
   #iscale = 20,
&cntrl
   nmropt = 1, pencut=-0.001,
   igb = 1, ntb = 0, ntt = 1,
   ntpr = 50, ntwx = 50,
   cut = 15.0, 
   nstlim=30000, imin = 0, vlimit = 20, 
   extdiel=3.9, gbsa=1,
   ntc=2, 
   ntf=2, 
   dt=0.001
&end

#Simple simulated annealing algorithm:                                         
#                                                                              
#from steps 0 to 5000	: heat the system to 500K with highest gradient
#from steps 5001-12000	: equilibrate system @ 500K  with gradually increasing tautp (Temperature coupling)
#from steps 12001-25000	: re-cool to low temperatures with more increasing tautp (Temperature coupling)
#from steps 25001-30000	: final cooling with gradually shorter tautp

&wt type='TEMP0', istep1=0,istep2=10000,value1=500., value2=500., &end
&wt type='TEMP0', istep1=10001, istep2=14000, value1=500.0, value2=000.0, &end 
&wt type='TEMP0', istep1=14001, istep2=30000, value1=0.0, value2=0.0, &end
&wt type='TAUTP', istep1=0,istep2=5000,value1=0.4, value2=0.6, &end
&wt type='TAUTP', istep1=5001,istep2=10000,value1=0.6, value2=1.0, &end
&wt type='TAUTP', istep1=10001,istep2=14000,value1=3.0, value2=2.0, &end
&wt type='TAUTP', istep1=14001,istep2=25000,value1=2.0, value2=3.0, &end
&wt type='TAUTP', istep1=25001,istep2=30000,value1=3.0, value2=0.01, &end
                                                                               
# "Ramp up" the restraints over the first 3000 steps

&wt type='REST', istep1=0,istep2=3000,value1=0.1,value2=1.0, &end                                                  
&wt type='REST', istep1=3000,istep2=29998,value1=1.0, value2=1.0, &end                                                  
&wt type='IMPROP', istep1=0,istep2=29998,value1=1, value2=50, &end
&wt type='BOND', istep1=0,istep2=29998,value1=1, value2=1, &end
&wt type='ANGLE', istep1=0,istep2=29998,value1=1, value2=1,&end
&wt type='TORSION', istep1=0,istep2=29998,value1=1, value2=1, &end
&wt type='VDW', istep1=0,istep2=29998,value1=1, value2=1 &end                                      

&wt type='END'  &end
LISTOUT=POUT
DISANG=../RST

EOF

########################################################
########################################################

	printf "Create prmtop and input coordinates for no. $start_no ...              \n"
	
	tleap -f tleap.in

	printf "Minimizing structure no. $start_no ...                                 \n"

	sander \
		-O -p prmtop -i min.in \
		-c start.inpcrd \
		-o $start_no\_min.out \
		-r $start_no\_min.crd 
	

	printf "Annealing structure no. $start_no ...                                 \n"
	
	sander \
		-O -p prmtop -i anneal.in \
		-c $start_no\_min.crd \
		-o ../$start_no\_anneal.out \
		-r $start_no\_anneal.crd 

 	echo "..."
	echo "generating a pdb..."
	echo "..."
	ambpdb -bres -p prmtop -c $start_no\_anneal.crd \
                                     > $start_no\_anneal.pdb

        mv  $start_no\_anneal.pdb ../$start_no\_anneal.pdb
 
	
	@ start_no = ($start_no + 1)
 	rm -f mdcrd

	cd ..
end

printf "\ndone.\n"
